from collections import Counter


inventory_cfg = [
    {
        'name': 'Red Widget',
        'code': 'R01',
        'price': 32.95,
    },
    {
        'name': 'Green Widget',
        'code': 'G01',
        'price': 24.95,
    },
    {
        'name': 'Blue Widget',
        'code': 'B01',
        'price': 7.95,
    },
]
promotion_cfg = [
    {
        'rule_name': 'buy_one_get_second_half_price',
        'target_products': [
            'R01',
        ],
    },
]


class Product:
    def __init__(self, code, name, price):
        self.code = code
        self.name = name
        self.price = price


class Singleton:
    def __init__(self, cls):
        self._cls = cls

    def Instance(self):
        try:
            return self._instance
        except AttributeError:
            self._instance = self._cls()
            return self._instance


@Singleton
class ProductInventory:
    def __init__(self):
        self._widgets = {}
        self._init_inventory(inventory_cfg)

    def _init_inventory(self, inventory):
        for product in inventory:
            self._load_product(**product)

        return

    def _load_product(self, code, name, price, **product):
        self._widgets[code] = Product(code, name, price)

    def get_price(self, product):
        if product not in self._widgets:
            raise ValueError('Unknown product code')

        return self._widgets[product].price

    def show_inventory(self):
        for item in self._widgets.values():
            print(item.code, item.name, item.price)


@Singleton
class Promotion:
    def __init__(self):
        self._promotions = {}
        self._load_promotion_rules(promotion_cfg)
        self._handlers = {}

    def add_promo_handler(self, rule_name, handler):
        self._handlers[rule_name] = handler

    def get_promo_handler(self, rule_name):
        handler = self._handlers[rule_name]

        if not handler:
            raise KeyError('No such promotion: {}'.format(rule_name))

        return handler

    def _load_promotion_rules(self, promotion_cfg):
        for promotion_detail in promotion_cfg:
            self._load_one_promotion(**promotion_detail)

        return

    def _load_one_promotion(self,
                            rule_name,
                            target_products,
                            **promotion_detail):
        self._promotions[rule_name] = target_products

    def get_amount(self, basket_items):
        total = 0

        for get_promotion_value, target_products in self._promotions.items():
            total += get_promotion_value(target_products, basket_items)

        return total

    def buy_one_get_second_half_price(self,
                                      product,
                                      target_products,
                                      basket_items):
        if product not in target_products:
            return 0

        count = Counter(basket_items)
        num_refund_item = (count[product] // 2) * 1.5 + (count[product] % 2)

        return num_refund_item


class ShoppingBasket:
    def __init__(self, items):
        self._items = items
        self._product = ProductInventory.Instance()
        self._promotion = Promotion.Instance()
        self._promotion.add_promo_handler(
            'buy_one_get_second_half_price',
            self._promotion.buy_one_get_second_half_price)

    def get_price(self):
        item_total_price = self.get_items_price()
        offer_adjustment = self.get_offer_amount()
        shipping_cost = self.get_shipping_cost(item_total_price -
                                               offer_adjustment)
        item_total_price += shipping_cost - offer_adjustment

        return self.last_2_digits(item_total_price)

    def last_2_digits(self, f_num):
        return int(f_num * 100) / 100

    def get_items_price(self):
        total = 0

        for item_code in self._items:
            total += self._product.get_price(item_code)

        return total

    def show_cart(self):
        print('{} {}'.format(self._items, self.get_price()))

    def get_offer_amount(self):
        total = 0
        count = Counter(self._items)

        for promo_rule, target_products in self._promotion._promotions.items():
            for item in set(self._items):
                if item not in target_products:
                    continue

                promo_handler = self._promotion.get_promo_handler(promo_rule)
                num_refund_item = promo_handler(item,
                                                target_products,
                                                self._items)
                total += (count[item] - num_refund_item) * \
                    self._product._widgets[item].price

        return total

    def get_shipping_cost(self, item_total_price):
        shipping_cost = 0

        if item_total_price >= 90:
            shipping_cost = 0
        elif item_total_price >= 50:
            shipping_cost = 2.95
        else:
            shipping_cost = 4.95

        return shipping_cost


def driver():
    basket1 = ShoppingBasket(['R01', 'R01'])
    basket2 = ShoppingBasket(['B01', 'G01'])
    basket3 = ShoppingBasket(['R01', 'R01', 'R01', 'B01', 'B01'])
    basket4 = ShoppingBasket(['R01', 'R01', 'R01', 'R01'])
    basket5 = ShoppingBasket(['R01', 'G01'])
    basket1.show_cart()
    basket2.show_cart()
    basket3.show_cart()
    basket4.show_cart()
    basket5.show_cart()


if __name__ == '__main__':
    driver()
