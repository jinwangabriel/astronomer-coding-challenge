# Astronomer Take Home Code Test

----
## usage
* python3.7 ShoppingCart.py

----
## design
1. ProductInventory and Promotion are classes to load the list of product
related information and promotion rules and its offer handlers. Both will read
from cfg(defined at the top of the file), which are easily extensive.
2. ShoppingBasket is the main class of this program that calculates the
shopping cost, special discount and shipping cost.

----
## assumption
1. When external APIs like 'add\_item\_into\_basket', it would use product code
to add item, e.g.('R01')
